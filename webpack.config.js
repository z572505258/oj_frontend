const resolve = require('path').resolve;
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const url = require('url');
const fs=require('fs');


let dev_proxy = {};
dev_proxy["/api/"] = "http://222.27.161.245:9001";
dev_proxy["/images/"] = "http://222.27.161.245:9001";
dev_proxy["/api/ws/"] = {
    target: 'ws://222.27.161.245:9001',
    ws: true
};

module.exports = (options = {}) => ({
    entry: {
        vendor: './src/vendor',
        index: './src/main.js'
    },
    output: {
        path: resolve(__dirname, '../oj_backend/output/public/frontend'), //编译生成的文件目录
        publicPath: options.dev ? '/assets/' : '/', //网站根目录，用于在引入js文件时加上前缀。比如引入static/main.js会写成[publicPath]/static/main.js
        filename: options.dev ? '[name].js' : 'static/[name].js?[chunkhash]', //和rootPath连起来的
        chunkFilename: options.dev ? '[id].js?[chunkhash]' : 'static/[id].js?[chunkhash]',
    },
    module: {
        rules: [{
            test: /\.vue$/,
            use: ['vue-loader']
        },
            {
                test: /\.js$/,
                use: ['babel-loader'],
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader', 'postcss-loader']
            },
            {
                test: /\.(png|jpg|jpeg|gif|eot|ttf|woff|woff2|svg|svgz)(\?.+)?$/,
                use: [{
                    loader: 'url-loader',
                    options: {
                        limit: 10000
                    }
                }]
            }
        ]
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            names: ['vendor', 'manifest']
        }),
        new HtmlWebpackPlugin({
            template: 'src/index.html'
        })
    ],
    resolve: {
        alias: {
            '~': resolve(__dirname, 'src'),
            'vue$': 'vue/dist/vue.js'
        }
    },
    devServer: {
        host: 'localhost',
        port: 9000,
        proxy: dev_proxy,
        historyApiFallback: {
            index: url.parse(options.dev ? '/assets/' : '/').pathname
        }
    },
    devtool: options.dev ? '#eval-source-map' : '#source-map'
});