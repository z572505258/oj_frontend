import NotFound from './views/404.vue'
import Home from './views/Home.vue'
import Problems from './views/Problems.vue'
import Problem from './views/Problem.vue'
import Status from './views/Status.vue'
import Index from './views/Index.vue'
import Contest from './views/Contest/Contest.vue'
import Contests from './views/Contest/Contests.vue'


let rootPath = "/";


let routes = [
    {
        path: rootPath,
        component: Home,
        redirect: {path: rootPath + 'index'},
        children: [
            {
                path: 'index',
                component: Index,
            },
            {
                path: 'problem',
                component: Problem
            },
            {
                path: 'problems',
                component: Problems,
            },
            {
                path: 'status',
                component: Status
            },
            {
                path: 'contests',
                component: Contests
            },
            {
                path: 'contest',
                component: Contest
            },
        ]
    },
    {
        path: rootPath + '/404',
        component: NotFound,
        name: '',
        hidden: true
    },
    {
        path: rootPath + '/*',
        hidden: true,
        redirect: {path: rootPath + '/404'}
    }
];

export default routes;