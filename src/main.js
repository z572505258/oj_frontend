import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import VueRouter from 'vue-router'
import routes from './routes'
import ContestCreateEditPane from './views/Contest/ContestCreateEditDialog.vue'
import ProblemDetailPane from './views/ProblemDetailPane.vue'

Vue.use(ElementUI);
Vue.use(VueRouter);

Vue.component("contest-edit-dialog", ContestCreateEditPane);
Vue.component("problem-detail-pane", ProblemDetailPane);

// var ace = require('vue2-ace-editor');
// require('vue2-ace-editor/node_modules/brace/mode/html');
// require('vue2-ace-editor/node_modules/brace/mode/javascript');
// require('vue2-ace-editor/node_modules/brace/mode/less');
// require('vue2-ace-editor/node_modules/brace/theme/chrome');

const router = new VueRouter({
    mode: 'history',
    routes,
});

new Vue({
    router
}).$mount('#app');
