var SIGN_REGEXP = /([yMdhsm])(\1*)/g;
var DEFAULT_PATTERN = 'yyyy-MM-dd';
const crypto = require('crypto');
function padding(s, len) {
    var len = len - (s + '').length;
    for (var i = 0; i < len; i++) { s = '0' + s; }
    return s;
};

var documentElement, isIE;

if (!('localStorage' in window)) {
    alert("aaaaaaa");
    window.localStorage = (function() {
        var documentElement, isIE = !!document.all;

        if (isIE) {
            documentElement = document.documentElement;
            documentElement.addBehavior('#default#userdata');
        }

        return {
            setItem: function(key, value) {
                if (isIE) {
                    documentElement.setAttribute('value', value);
                    documentElement.save(key);
                }
                else {
                    window.globalStorage[location.hostname][key] = value;
                }
            },
            getItem: function(key) {
                if (isIE) {
                    documentElement.load(key);
                    return documentElement.getAttribute('value');
                }

                return window.globalStorage[location.hostname][key];
            },
            removeItem: function(key) {
                if (isIE) {
                    documentElement.removeAttribute('value');
                    documentElement.save(key);
                }
                else {
                    window.globalStorage[location.hostname].removeItem(key);
                }
            }
        };
    })();

}
console.log("util init");


export default {
    HTMLEncode: function (html) {
        var temp = document.createElement("div");
        (temp.textContent != null) ? (temp.textContent = html) : (temp.innerText = html);
        var output = temp.innerHTML;
        temp = null;
        return output;
    },
    sha1: function (str) {
        var sha1 = crypto.createHash('sha1');
        sha1.update('572505258@qq.com');
        sha1.update(str);
        return sha1.digest('hex');
    },
    getQueryStringByName: function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        var r = window.location.search.substr(1).match(reg);
        var context = "";
        if (r != null)
            context = r[2];
        reg = null;
        r = null;
        return context == null || context == "" || context == "undefined" ? "" : context;
    },
    queryString : {
        parse : function (str) {
            if (typeof str !== 'string') {
                return {};
            }

            str = str.trim().replace(/^\?/, '');

            if (!str) {
                return {};
            }

            return str.trim().split('&').reduce(function (ret, param) {
                var parts = param.replace(/\+/g, ' ').split('=');
                var key = parts[0];
                var val = parts[1];

                key = decodeURIComponent(key);
                // missing `=` should be `null`:
                // http://w3.org/TR/2012/WD-url-20120524/#collect-url-parameters
                val = val === undefined ? null : decodeURIComponent(val);

                if (!ret.hasOwnProperty(key)) {
                    ret[key] = val;
                } else if (Array.isArray(ret[key])) {
                    ret[key].push(val);
                } else {
                    ret[key] = [ret[key], val];
                }

                return ret;
            }, {});
        },

        stringify : function (obj) {
            return obj ? Object.keys(obj).map(function (key) {
                var val = obj[key];

                if (Array.isArray(val)) {
                    return val.map(function (val2) {
                        return encodeURIComponent(key) + '=' + encodeURIComponent(val2);
                    }).join('&');
                }

                return encodeURIComponent(key) + '=' + encodeURIComponent(val);
            }).join('&') : '';
        },

        push : function(key, new_value) {
            var params = this.parse(location.search);
            params[key] = new_value;
            var new_params_string = this.stringify(params)
            history.pushState({}, "", window.location.pathname + '?' + new_params_string);
        }
    },


    setLocalStorage: function(key, value) {
        if (isIE) {
            documentElement.setAttribute('value', value);
            documentElement.save(key);
        }
        else {
            window.globalStorage[location.hostname][key] = value;
        }
    },
    getLocalStorage: function(key) {
        if (isIE) {
            documentElement.load(key);
            return documentElement.getAttribute('value');
        } else {
            return window.globalStorage[location.hostname][key];
        }
    },
    removeItem: function(key) {
        if (isIE) {
            documentElement.removeAttribute('value');
            documentElement.save(key);
        }
        else {
            window.globalStorage[location.hostname].removeItem(key);
        }
    },
    setCookie: function (key, value, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        //console.info(key + "=" + value + "; " + expires);
        document.cookie = key + "=" + value + "; " + expires;
        //console.info(document.cookie);
    },
    //获取cookie
    getCookie: function (key) {
        var name = key + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
        }
        return "";
    },
    //清除cookie
    clearCookie: function (key) {
        this.setCookie(key, "", -1);
    },


    formatDate: {
        format: function (date, pattern) {
            pattern = pattern || DEFAULT_PATTERN;
            return pattern.replace(SIGN_REGEXP, function ($0) {
                switch ($0.charAt(0)) {
                    case 'y': return padding(date.getFullYear(), $0.length);
                    case 'M': return padding(date.getMonth() + 1, $0.length);
                    case 'd': return padding(date.getDate(), $0.length);
                    case 'w': return date.getDay() + 1;
                    case 'h': return padding(date.getHours(), $0.length);
                    case 'm': return padding(date.getMinutes(), $0.length);
                    case 's': return padding(date.getSeconds(), $0.length);
                }
            });
        },
        parse: function (dateString, pattern) {
            var matchs1 = pattern.match(SIGN_REGEXP);
            var matchs2 = dateString.match(/(\d)+/g);
            if (matchs1.length == matchs2.length) {
                var _date = new Date(1970, 0, 1);
                for (var i = 0; i < matchs1.length; i++) {
                    var _int = parseInt(matchs2[i]);
                    var sign = matchs1[i];
                    switch (sign.charAt(0)) {
                        case 'y': _date.setFullYear(_int); break;
                        case 'M': _date.setMonth(_int - 1); break;
                        case 'd': _date.setDate(_int); break;
                        case 'h': _date.setHours(_int); break;
                        case 'm': _date.setMinutes(_int); break;
                        case 's': _date.setSeconds(_int); break;
                    }
                }
                return _date;
            }
            return null;
        }
    }

};
